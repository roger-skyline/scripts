#!/bin/bash

# Merge dev branch with master and push
# execute a git pul on each worker
# execute a diff between preprod worker and master worker
#TODO: different path for prod-workers

library_path="../.."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ] &&
	[ -f "${library_path}/shell_library/variable_functions.sh" ] &&
	[ -f "${library_path}/shell_library/edit_functions.sh" ] &&
	[ -f "${library_path}/shell_library/system_functions.sh" ] &&
	[ -f "${library_path}/file_parsing/array_check.sh" ] &&
	[ -f "${library_path}/file_parsing/array_extract.sh" ]
then
	. ${library_path}/shell_library/variable_functions.sh
	. ${library_path}/shell_library/type_functions.sh
	. ${library_path}/shell_library/edit_functions.sh
	. ${library_path}/shell_library/system_functions.sh
	. ${library_path}/file_parsing/array_check.sh
	. ${library_path}/file_parsing/array_extract.sh
else
	echo "Can't load library files, abort"
	exit 1
fi

# Trap CTRL C and execute gracefulExit when catch it
trap gracefulExit INT

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Check if one argument have been provide
if [ ${#1} -eq 0 ]
then
	echo "No argument was provided, abort"
	exit 1
fi

# Check if argument given if a file
if [ ! -f "$1" ]
then
	echo "No valid filename given."
	exit 1
fi

DEBUG=2
new_file="backup_ini_conf.clean"
log_file="backup_ini.log"

# Declare all valid keywork
declare -a keywords
keywords=("username" "remote-path" "backup-size" "conf-path")
keywords_array=("hosts" "files")
keywords_mand=("username" "remote-path" "backup-size" "conf-path" "hosts" "files")

# Check if argument
if [ ${#1} -eq 0 ]
then
	echo "No argument was provided, abort"
	exit 1
fi

# Check if file given exist
if [ -d "$1" ]
then
	echo "No valid filename given."
	exit 1
fi

# Remove duplicate and create array to compare the 2 data contents
no_duplicate=$(cat "$1" | sort -u | sed '/^\s*$/d')
readarray -t no_dupli_arr <<<"$no_duplicate"
file_content=$(cat "$1" | sort | sed '/^\s*$/d')
readarray -t file_arr <<<"$file_content"

# Return diff between 2 arrays
array_diff=$(printf '%s\n' "${no_dupli_arr[@]}" "${file_arr[@]}" | sort | uniq -u)

# If duplicate, create new file with clean lines
if [ ${#array_diff} -eq 0 ]
then
	echo -e ${RED}"Duplicate find in file, create fresh new file"${NC}
	echo "$no_duplicate" > "$1.no"
	# Put all line in array, and trim
	readarray file_content_arr < "$1".no
else
	readarray file_content_arr < "$1"
fi

# Unset unused variables
unset no_duplicate file_content

declare -a file_content

# Delele and recreate new file for push clean line into
if [ -f ${new_file} ]
then
	rm ${new_file}
fi
touch ${new_file}

# Parse file given in parameter, line by line
for i in "${!file_content_arr[@]}"
do
	check_next_line "${file_content_arr[$i]}"
done

# If $keywords_mand array contain at least one value
# Not all mandatory keywords have been provide so abort
if [ "${#keywords_mand[@]}" != 0 ]
then
	echo -e ${RED}"Not all mandatory keywords have been provided, missing:"
	echo -e ${keywords_mand[@]}${NC}
	exit 1
fi

# if check_next_line has never exit script due to bad line format in argv file,
# We get clean array from new file created
readarray clean_array < ${new_file}

# Now we get back variable from argument file
keyword='hosts'
if ft_valid_keyword "${keyword}"
then
	hosts_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

# Now we get back variable from argument file
keyword='files'
if ft_valid_keyword "${keyword}"
then
	files_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

# Now we get back variable from argument file
keyword='username'
if ft_valid_keyword "${keyword}"
then
	username_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Now we get back variable from argument file
keyword='remote-path'
if ft_valid_keyword "${keyword}"
then
	remote_path_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Now we get back variable from argument file
keyword='backup-size'
if ft_valid_keyword "${keyword}"
then
	backup_size_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# Now we get back variable from argument file
keyword='conf-path'
if ft_valid_keyword "${keyword}"
then
	conf_path_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# if DEBUG>0 show all key and values getting form file
if [ $DEBUG -gt 0 ]
then
	echo -e ${YEL}"Show all val from argument file"
	for i in "${!keywords[@]}"
	do
		value=$(ft_get_value "${keywords[$i]}" "${clean_array[@]}")
		echo ${keywords[$i]}": $value"
	done
	echo -e "${NC}"
fi

# Get all values from array
hosts_arr=($(ft_get_value_arr "${hosts_val}"))

# Get all values from array
files_arr=($(ft_get_value_arr "${files_val}"))

# Check :
# - ipv4 host
# - presence of username or backup
# - presence of backup-path
for i in ${!hosts_arr[@]}
do
	if ! ft_is_ipv4 "${hosts_arr[$i]}"
	then
		echo "Bad IPv4 for host: '${hosts_arr[$i]}', abort"
		exit 1
	fi
	# test ssh connection to hosts
	echo -e ${YEL}ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no root@"${hosts_arr[$i]}" 'exit 2>&1'${NC}
	ssh_return=$(ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no root@"${hosts_arr[$i]}" "exit 2>&1")

	# If ssh_return != "" abort
	if [ $? -ne 0 ]
	then
		# If DEBUG>0 print info
		if [ $DEBUG -gt 0 ]
		then
			echo "ssh return:"
			echo "$ssh_return"
		fi
		echo -e ${RED}"Can't connect to host via '${hosts_arr[$i]}' IPv4, abort"${NC}
		exit 1
	fi
	echo "ssh to host: '${hosts_arr[$i]}': OK"

	# Add user to backup host
	echo -e ${YEL}"ssh root@${hosts_arr[$i]} adduser ${username_val} --home /home/${username_val} --shell /bin/bash --gecos 'First Last,RoomNumber,WorkPhone,HomePhone' --disabled-password 2>&1"${NC}
	ssh_adduser=$(ssh root@"${hosts_arr[$i]}" "adduser ${username_val} --home /home/${username_val} --shell /bin/bash --gecos 'First Last,RoomNumber,WorkPhone,HomePhone' --disabled-password 2>&1")

	# check return of adduser command
	if [ "$(echo "$ssh_adduser" |  grep 'Adding user')" == "" ] && [ "$(echo "$ssh_adduser" |  grep 'already exists')" == "" ]
	then
		echo -e ${RED}"Can't add user: '${username_val}' to '${hosts_arr[$i]}', abort"${NC}
		exit 1
	elif [ "$(echo "$ssh_adduser" |  grep 'success')" == "" ]
	then
		# Set a password and check return
		echo -e ${YEL}ssh root@${hosts_arr[$i]} "echo -e '${username_val}:backup' | sudo chpasswd ${username_val}"${NC}
		ssh_chg_passwd=$(ssh root@"${hosts_arr[$i]}" "echo -e '${username_val}:backup' | sudo chpasswd ${username_val}")

		if [ "$?" -ne 0 ]
		then
			echo -e ${RED}"Can't change '${username_val}' password, abort"${NC}
			exit 1
		fi
	fi

	# Check if remote path exist
	echo -e ${YEL}ssh root@"${hosts_arr[$i]}" stat $remote_path_val \> /dev/null 2\>\&1${NC}
	path_exist=$(ssh root@"${hosts_arr[$i]}" stat $remote_path_val \> /dev/null 2\>\&1)

	# If not exist, create it
	if [ "$?" -ne 0 ]
	then
		echo -e ${ORA}"Path '$remote_path_val' doesn't exist on remote host '${hosts_arr[$i]}', will create it"${NC}

		# Try to create path
		echo -e ${YEL}"ssh root@${hosts_arr[$i]} mkdir ${remote_path_val}"${NC}
		ssh_create_path=$(ssh root@${hosts_arr[$i]} mkdir ${remote_path_val})

		# Print ssh return
		if [ $? != 0 ]
		then
			echo "$ssh_create_path"
		fi
	fi
	echo "remote path on host '${hosts_arr[$i]}': OK"

	# Check if can connect with rsa key
	echo -e ${YEL}"ssh -o PasswordAuthentication=no ${username_val}@${hosts_arr[$i]} exit &>/dev/null"${NC}
	ssh -o PasswordAuthentication=no ${username_val}@${hosts_arr[$i]} exit &>/dev/null

	if [ "$(test $? == 0 && echo 'true' || echo 'false')" == "false" ]	
	then
		# Check presence of ssh_rsa_connection.sh file
		if [ -f "/home/bbichero/a711e2eda110c1ca7ad40179ca34d030/ssh_rsa_connection.sh" ]
		then
			# id_rsa.pub exchange for ssh connection
			/home/bbichero/a711e2eda110c1ca7ad40179ca34d030/ssh_rsa_connection.sh -u ${username_val} -i ${hosts_arr[$i]} -s "backup"
		else
			echo "Can't find ssh_rsa_connection.sh file, please check path, abort"
			exit 1
		fi
	else
		echo "ssh connection with rsa key has succed"
	fi
	echo "ssh ${username_val}@${hosts_arr[$i]}: OK"
done
