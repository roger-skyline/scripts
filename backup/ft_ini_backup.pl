#!/usr/bin/perl

use strict;
use warnings;
use JSON::XS;
#use JSON; # imports encode_json, decode_json, to_json and from_json.
#use Cpanel::JSON::XS qw(encode_json decode_json);
#use Data::Dumper;
#use JSON;

#my $json = new JSON->new->utf8;
my $json = JSON::XS->new;

# Get back argument number
my $num_args = $#ARGV + 1;

# If argument number != 1, exit with error
if ($num_args != 1)
{
	print "Usage: ft_ini_backup.pl host.json\n";
	exit 1;
}

# Get agument name in variable
my $filename = $ARGV[0];

# Check if filename from argument can be open
open(my $fh, '<:encoding(UTF-8)', $filename)
	or die "Could not open file '$filename' $!";

# Get every line from file and stock them
my $json_str = "";
while (my $row = <$fh>)
{
	$json_str .= $row;
}

my $object = $json->utf8->decode($json_str);

# Store json in object
if (!$object)
{
	$json->incr_reset;
	print  "expected JSON object or array at beginning of string\n";
	exit 1;
}

#for my $item ($object)
#keys $object; # reset the internal iterator so a prior each() doesn't affect the loop
while ( my($key, $value) = each %{$object})
{
	print 'hostname: ', $key, "\n";

	# Check ping of each hostname

	while ( my($key2, $value2) = each %{$value})
	{
		# Check if keyword is valid
		print $key2, " :", $value2, "\n";
	}
}

# Reset data decode by JSON object
$json->incr_reset;
