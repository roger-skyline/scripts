#!/bin/bash

###       Backup Mysql        ###
#                               #
#  Will be executed every :     #
#                               #
#  - Master Backup              #
#  - Re-sync replication        #
#  - Error occur on slave or    #
#    master                     #
#                               #
#  Execute script on slave

function is_ipv4()
{
	if [[ $1 =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]
	then
    		return 1
	else
		return 0;
	fi
}

function is_found()
{
	if ! [ -f $2 ]
	then
		echo "File not found."
		exit 1
	fi
	if [ ${#1} -eq 0 ]
	then
		echo "No partern given."
		exit 1
	fi
	if grep -q $1 $2
	then
		return 1
	else
		return 0
	fi
}

dbflag=false

# Default Configuration
HOST=''
NO_DATA=''
MASTER=''
DATE=''

USER_REMOTE=slave_dump
PASS_REMOTE=slave-to-master-dump

USER_REPLI=sky-replication
PASS_REPLI=replication-mariadb

USER_LOCAL=master
PASS_LOCAL=mariadb-master

#set -o errexit -o nounset -o pipefail
TEMP="$(getopt -o d:h:ni::mtc -l db:,host:,no-data,import::,master,date,cmp -n "$0" -- "$@")"
eval set -- "$TEMP"

# extract options and their arguments into variables.
while true ; do
	case "$1" in
		-d|--db)
			DB="$2" ; dbflag=true ; shift 2 ;;
		-h|--host)
		case "$2" in
			"")
				HOST="" ; shift 2 ;;
			"localhost")
				HOST="" ; shift 2 ;;
			*)
				if is_ipv4 "$2"
				then
    					echo -e "Error: Bad IPv4 given.";
					exit 1
				fi
				HOST="$2" ; shift 2 ;;
		esac ;;
		-n|--no-data) NO_DATA="--no-data" ; shift ;;
		-i|--import)
		case "$2" in
			"")
				IMPORT=true ; shift 2 ;;
			*)
				IMPORT="$2" ; shift 2 ;;
		esac ;;
		-m|--master)
			MASTER="--master-data" ; shift ;;
		-t|--date)
			DATE="_$(date +%Y_%m_%d_%HH%MM%S)" ; shift ;;
    		c|--cmp)
			CMP=1 ; shift ;;
		\?)
    			echo "Invalid option: -$OPTARG" >&2 ; exit 1 ;;
  		:)
    			echo "Option -$OPTARG requires an argument." >&2 ; exit 1 ;;
		--) shift ; break ;;
		*)
			echo "Error: usage ./mysql_backup.sh --db=<database>">&2 ; exit 1 ;;
	esac
done

# Check if mandatory argument is define
if ! ${dbflag}
then
	echo "Error: usage ./mysql_backup.sh --db=<database>"
	exit 1
fi

# Check if a db.sql or db${DATE}.sql exist
if [ -f db${DATE} ]
then
	FILE=db_new${DATE}.sql
elif [ -f db_new${DATE}.sql ]
then
	rm -v db_new${DATE}.sql
	FILE=db_new${DATE}.sql
else
	FILE=db${DATE}.sql
fi

# Execute mysql dump command
#-t --insert-ignore --skip-opt
if [ -f mysql_dump.log ] ;then
	rm mysql_dump.log
fi
if [ ${#HOST} -ne 0 ]
then
	echo "mysqldump --databases ${DB} ${NO_DATA} ${MASTER} --single-transaction -u${USER_REMOTE} -p${PASS_REMOTE} -h${HOST} > $FILE"
	mysqldump --databases ${DB} ${NO_DATA} ${MASTER} --single-transaction -u${USER_REMOTE} -p${PASS_REMOTE} -h${HOST} 2>>mysql_dump.log > $FILE
else
	echo "mysqldump --databases ${DB} ${NO_DATA} ${MASTER} --single-transaction -u${USER_LOCAL} -p${PASS_LOCAL} > $FILE"
	mysqldump --databases ${DB} ${NO_DATA} ${MASTER} --single-transaction -u${USER_LOCAL} -p${PASS_LOCAL} 2>>mysql_dump.log > $FILE
fi
# Check return of mysqldump
if ! is_found "error" mysql_dump.log
then
	cat mysql_dump.log
	rm $FILE
	exit 1
fi

# If -m argument is provide change line CHANGE MASTER TO to add
# other option for full replication
if [ ${#MASTER} -ne 0 ]
then
	MASTER_CONF=$(grep -o -E "CHANGE MASTER TO MASTER_LOG_FILE='mysql-bin.[0-9]{1,}', MASTER_LOG_POS=[0-9]{1,}" $FILE)
	# Check return of previous grep
	if [ ${#MASTER_CONF} -eq 0 ]
	then
		echo "Error in MASTER CONF, database select, must not be master"
		exit 1
	fi

	NEW_CONF="STOP SLAVE;${MASTER_CONF}, MASTER_HOST='${HOST}', MASTER_USER='${USER_REPLI}', MASTER_PASSWORD='${PASS_REPLI}';START SLAVE"
	echo sed -i -e s/"$MASTER_CONF"/"$NEW_CONF"/ $FILE
	sed -i -e s/"$MASTER_CONF"/"$NEW_CONF"/ $FILE
fi

# Execute import of databases
if [ ${#IMPORT} -ne 0 ]
then
	if [ ${#CMP} -ne 0 ]
	then
		echo "mysql -u normal_user -psalve-mysql-pass -B -e DROP DATABASE IF EXISTS ${DB}_cmp"
		mysql -u normal_user -pslave-mysql-pass -B -e "DROP DATABASE IF EXISTS ${DB}_cmp"
		echo "sed -i -e s/\`${DB}\`/\`${DB}_cmp\`/ $FILE"
		sed -i -e s/\`${DB}\`/\`${DB}_cmp\`/ $FILE
		echo "mysql -u normal_user -pslave-mysql-pass < $FILE"
		mysql -u normal_user -pslave-mysql-pass 2>>mysql_import.log < $FILE
		rm -v $FILE
	else
		echo "mysql -u normal_user -pslave-mysql-pass < $FILE"
		mysql -u normal_user -pslave-mysql-pass 2>>mysql_import.log < $FILE
	fi
fi
