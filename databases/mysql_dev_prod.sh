#!/bin/bash
#TODO: check if database in conf file exist in db
#TODO: check ssh cnnection ssh rsa key

# Before executing file be sure :
# - ${prod_host_db_val}_cmp database exist
# - ${mysql_prod_user_val} have ALL PRIVILEGES for ${prod_host_db_val} and _cmp database


library_path="../.."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ] &&
	[ -f "${library_path}/shell_library/variable_functions.sh" ] &&
   	[ -f "${library_path}/shell_library/edit_functions.sh" ] &&
   	[ -f "${library_path}/file_parsing/array_check.sh" ] &&
   	[ -f "${library_path}/file_parsing/array_extract.sh" ]
then
    # Include library
    . ${library_path}/shell_library/variable_functions.sh
    . ${library_path}/shell_library/type_functions.sh
    . ${library_path}/shell_library/edit_functions.sh
    . ${library_path}/file_parsing/array_check.sh
    . ${library_path}/file_parsing/array_extract.sh
else
    echo "Can't load library files, abort"
    exit 1
fi

# Trap CTRL C adn execute gracefulExit when catch it
trap gracefulExit INT

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

DEBUG=2
new_file="arguments.clean"
log_file="create_arch.log"

# Declare all valid keywork
declare -a keywords
keywords=("dev-host-user" "mysql-dev-host" "mysql-dev-user" "mysql-dev-passwd" "mysql-dev-db" "prod-host-user" "mysql-prod-host" "mysql-prod-user" "mysql-prod-passwd" "mysql-prod-db" "mysql-cmp-db")
keywords_array=()
keywords_mand=("dev-host-user" "mysql-dev-host" "mysql-dev-user" "mysql-dev-passwd" "mysql-dev-db" "prod-host-user" "mysql-prod-host" "mysql-prod-user" "mysql-prod-passwd" "mysql-prod-db" "mysql-cmp-db")

# Check if argument
if [ ${#1} -eq 0 ]
then
	echo "No argument was provided, abort"
	exit 1
fi

# Check if file given exist
if [ ! -f "$1" ]
then
	echo "No valid filename given."
	exit 1
fi

# Remove duplicate and create array to compare the 2 data contents
no_duplicate=$(cat "$1" | sort -u | sed '/^\s*$/d')
readarray -t no_dupli_arr <<<"$no_duplicate"

file_content=$(cat "$1" | sort | sed '/^\s*$/d')
readarray -t file_arr <<<"$file_content"

# Return diff between 2 arrays
array_diff=$(printf '%s\n' "${no_dupli_arr[@]}" "${file_arr[@]}" | sort | uniq -u)

# If duplicate, create new file with clean lines
if [ ${#array_diff} -eq 0 ]
then
	echo -e ${RED}"Duplicate find in file, create fresh new file"${NC}
	echo "$no_duplicate" > "$1.no"
	# Put all line in array, and trim
	readarray file_content_arr < "$1".no
else
	readarray file_content_arr < "$1"
fi

# Unset unused variables
unset no_duplicate file_content

declare -a file_content

# Delele and recreate new file for push clean line into
if [ -f ${new_file} ]
then
	rm ${new_file}
fi
touch ${new_file}

# Parse file given in parameter, line by line
for i in "${!file_content_arr[@]}"
do
	check_next_line "${file_content_arr[$i]}"
done

# If $keywords_mand array contain at least one value
# Not all mandatory keywords have been provide so abort
if [ "${#keywords_mand[@]}" != 0 ]
then
	echo -e ${RED}"Not all mandatory keywords have been provided, missing:"
	echo -e ${keywords_mand[@]}${NC}
	exit 1
fi

# if check_next_line has never exit script due to bad line format in argv file,
# We get clean array from new file created
readarray clean_array < ${new_file}

# Now we execute each sript coresponding to keyword
# First we execute every mandatory keyword
# We fisrt check if keyword is valid

keyword='dev-host-user'
if ft_valid_keyword "${keyword}"
then
	dev_host_user_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-dev-host'
if ft_valid_keyword "${keyword}"
then
	mysql_dev_host_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-dev-user'
if ft_valid_keyword "${keyword}"
then
	mysql_dev_user_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-dev-passwd'
if ft_valid_keyword "${keyword}"
then
	mysql_dev_passwd_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-dev-db'
if ft_valid_keyword "${keyword}"
then
	mysql_dev_db_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='prod-host-user'
if ft_valid_keyword "${keyword}"
then
	prod_host_user_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-prod-host'
if ft_valid_keyword "${keyword}"
then
	mysql_prod_host_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-prod-user'
if ft_valid_keyword "${keyword}"
then
	mysql_prod_user_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-prod-passwd'
if ft_valid_keyword "${keyword}"
then
	mysql_prod_passwd_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-prod-db'
if ft_valid_keyword "${keyword}"
then
	mysql_prod_db_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='mysql-cmp-db'
if ft_valid_keyword "${keyword}"
then
	mysql_cmp_db_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# if DEBUG>0 show all key and values getting form file
if [ $DEBUG -gt 0 ]
then
	echo -e ${YEL}"Show all val from argument file"
	for i in "${!keywords[@]}"
	do
		value=$(ft_get_value "${keywords[$i]}" "${clean_array[@]}")
		echo ${keywords[$i]}": $value"
	done
	echo -e "${NC}"
fi

# Dump mysql database in preprod host
echo -e ${YEL}"ssh ${dev_host_user_val}@${mysql_dev_host_val} mysqldump --databases ${mysql_dev_db_val} --no-data --single-transaction -u ${mysql_dev_user_val} -p${mysql_dev_passwd_val} > /tmp/${mysql_dev_db_val}.sql"${NC}
ssh_dump_preprod=$(ssh ${dev_host_user_val}@${mysql_dev_host_val} mysqldump --databases ${mysql_dev_db_val} --no-data --single-transaction -u ${mysql_dev_user_val} -p${mysql_dev_passwd_val} > /tmp/${mysql_dev_db_val}.sql)

# Check presence of sql db dumped
echo -e ${YEL}"ssh ${dev_host_user_val}@${mysql_dev_host_val} ls /tmp/${mysql_dev_db_val}.sql"' > /dev/null; echo $?'${NC}
ssh_dump_return=$(ssh ${dev_host_user_val}@${mysql_dev_host_val} "ls /tmp/${mysql_dev_db_val}.sql"' > /dev/null; echo $?')

# Check if database was correctly dumped in it destination directory
if [ "${ssh_dump_return}" != 0 ]
then
	echo -e ${RED}"Error when execute mysqldump on remote '${mysql_dev_host_val}' host, abort"${NC}
	exit 1
fi
echo "Execute mysqldump from '${mysql_dev_host_val}': OK"

# Copy database dumped to mysql master prod host
echo -e ${YEL}"ssh ${dev_host_user_val}@${mysql_dev_host_val} scp /tmp/${mysql_dev_db_val}.sql ${prod_host_passwd_val}@${mysql_prod_host_val}:/tmp "';echo $?'" && rm /tmp/${mysql_dev_db_val}.sql"${NC}
ssh_scp_db=$(ssh ${dev_host_user_val}@${mysql_dev_host_val} "scp /tmp/${mysql_dev_db_val}.sql ${prod_host_passwd_val}@${mysql_prod_host_val}:/tmp "';echo $?'" && rm /tmp/${mysql_dev_db_val}.sql")

# Check return code of ssh_scp_db
if [ "${ssh_scp_db}" != 0 ]
then
	echo -e ${RED}"Error when copying database file from '${mysql_dev_host_val}' to '${mysql_prod_host_val}' host, abort"${NC}
	exit 1
fi
echo "Transfert preprod database: OK"

# Change ${mysql_dev_db_val} in sql file by ${mysql_cmp_db_val}
echo -e ${YEL}"ssh ${prod_host_user_val}@${mysql_prod_host_val} sed -i 's/${mysql_dev_db_val}/${mysql_cmp_db_val}/g' /tmp/${mysql_dev_db_val}.sql "'; echo $?'${NC}
ssh_sed_db=$(ssh ${prod_host_user_val}@${mysql_prod_host_val} "sed -i 's/${mysql_dev_db_val}/${mysql_cmp_db_val}/g' /tmp/${mysql_dev_db_val}.sql "'; echo $?') 

# Check return code of sed command
if [ "${ssh_sed_db}" != 0 ]
then
	echo -e ${RED}"Error when execute sed command, abort"${NC}
	exit 1
fi

# Import database copied from preprod host
echo -e ${YEL}"ssh ${prod_host_user_val}@${mysql_prod_host_val} mysql -u${mysql_prod_user_val} -p${mysql_prod_passwd_val} ${mysql_cmp_db_val} < /tmp/${mysql_dev_db_val}.sql "'; echo $?'${NC}
ssh_import_db=$(ssh ${prod_host_user_val}@${mysql_prod_host_val} "mysql -u${mysql_prod_user_val} -p${mysql_prod_passwd_val} ${mysql_cmp_db_val} < /tmp/${mysql_dev_db_val}.sql "'; echo $?')

# Check if database was correctly imported
if [ "${ssh_import_db}" != 0 ]
then
	echo -e ${RED}"Error when importing database on '${mysql_prod_host_val}' host, abort"${NC}
	exit 1
fi
echo "Import preprod database: OK"

# Execute mysql diff and put all sql request in file
echo -e ${YEL}"ssh ${prod_host_user_val}@${mysql_prod_host_val} mysqldiff --user=${mysql_prod_user_val} --password=${mysql_prod_passwd_val} ${mysql_prod_db_val} ${mysql_cmp_db_val} > diff.sql"'; echo $?'${NC}
ssh_diff_db=$(ssh ${prod_host_user_val}@${mysql_prod_host_val} "mysqldiff --user=${mysql_prod_user_val} --password=${mysql_prod_passwd_val} ${mysql_prod_db_val} ${mysql_cmp_db_val} > diff.sql"'; echo $?')

echo -e ${YEL}"ssh ${prod_host_user_val}@{mysql_prod_host_val} cat diff.sql"${NC}
ssh_content_diff=$(ssh ${prod_host_user_val}@${mysql_prod_host_val} cat diff.sql)

# If file now empty apply patch
if [ ${#ssh_content_diff} -eq 0 ]
then
	echo -e ${GRE}"Mysql diff file empty, nothing to do"${NC}
	exit 0
fi
echo "Create SQL diff patch: OK"

# Apply diff.sql file
echo -e ${YEL}"ssh ${prod_host_user_val}@${mysql_prod_host_val} mysql -u${mysql_prod_user_val} -p${mysql_prod_passwd_val} ${mysql_prod_db_val} < diff.sql "'; echo $?'${NC}
ssh_import_diff=$(ssh ${prod_host_user_val}@${mysql_prod_host_val} "mysql -u${mysql_prod_user_val} -p${mysql_prod_passwd_val} ${mysql_prod_db_val} < diff.sql "'; echo $?')

# Check return code
if [ ${ssh_import_diff} != 0 ]
then
	echo -e ${RED}"Error when importing sql diff patch, abort"${NC}
	exit 1
fi
echo "Apply SQL diff patch: OK"
