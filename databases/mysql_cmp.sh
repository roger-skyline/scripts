#!/bin/bash

function is_ipv4()
{
	if [[ $1 =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]
	then
    		return 1
	else
		return 0;
	fi
}

# Check if no argument
if [ $# -eq 0 ]
then
	echo "Error: usage ./mysql_cmp.sh --host=<ip> --db1=<database-1> --db2=<database-2>"
	exit 1
fi

# Configuration
USER_HOST_REMOTE=preprod-dump
PASS_HOST_REMOTE=preprod-dump-mariadb

USER_HOST_LOCAL=master
PASS_HOST_LOCAL=mariadb-master

# Note that we use `"$@"' to let each command-line parameter expand to a
# separate word. The quotes around `$@' are essential!
# We need TEMP as the `eval set --' would nuke the return value of getopt.
TEMP=`getopt -o h::d:b: -l host::,db1:,db2: -n 'mysql_cmp.sh' -- "$@"`
eval set -- "$TEMP"

# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -h|--host)
            	case "$2" in
                	"") HOST="" ; shift 2 ;;
			"localhost") HOST="" ; shift 2 ;;
                	*)
				if is_ipv4 $2
				then
    					echo -e "Error: Bad IPv4 given.";
					exit 1
				fi
				HOST="-h=$2" ; shift 2 ;;
            	esac ;;
        -d|--db1)
            	case "$2" in
                	"") shift 2 ;;
                	*) DB1=$2 ; shift 2 ;;
            	esac ;;
        -b|--db2)
            	case "$2" in
                	"") shift 2 ;;
                	*) DB2=$2 ; shift 2 ;;
            	esac ;;
	:) echo "Option -$OPTARG require an argument." >&2 ; exit 1 ;;
        --) shift ; break ;;
        *) echo "Error: usage ./mysql_cmp.sh --host=<ip> --db1=<database-1> --db2=<database-2>" ; exit 1 ;;
    esac
done

# If DB1 or DB2 have no parameter exit code
if [ ${#DB1} -eq 0 -o ${#DB2} -eq 0 ] || [ ${#DB1} -eq 0 -a ${#DB2} -eq 0 ]
then
	echo "Error, one or 2 database given is null"
	exit 1
fi

# Check locality of host
if ([ ${#HOST} -eq 0 ] || [ ${HOST} -eq 'localhost' ] || [ ${HOST$} -eq '127.0.0.1' ])
then
	USER=$USER_HOST_LOCAL
	PASS=$PASS_HOST_LOCAL
else
	USER=$USER_HOST_REMOTE
	PASS=$PASS_HOST_REMOTE
fi

# Apply mysqldiff function
echo "mysqldiff ${HOST} --user=${USER} --password=${PASS} ${DB1} ${DB2} > diff.sql"
mysqldiff ${HOST} --user=${USER} --password=${PASS} ${DB1} ${DB2} > diff.sql
