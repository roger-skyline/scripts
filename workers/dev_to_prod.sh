#!/bin/bash

# Merge dev branch with master and push
# execute a git pul on each worker
# execute a diff between preprod worker and master worker
#TODO: different path for prod-workers

library_path="../.."

# Check if library exist in current path
if [ -f "${library_path}/shell_library/type_functions.sh" ] &&
	[ -f "${library_path}/shell_library/variable_functions.sh" ] &&
	[ -f "${library_path}/shell_library/edit_functions.sh" ] &&
	[ -f "${library_path}/shell_library/system_functions.sh" ] &&
	[ -f "${library_path}/file_parsing/array_check.sh" ] &&
	[ -f "${library_path}/file_parsing/array_extract.sh" ]
then
	. ${library_path}/shell_library/variable_functions.sh
	. ${library_path}/shell_library/type_functions.sh
	. ${library_path}/shell_library/edit_functions.sh
	. ${library_path}/shell_library/system_functions.sh
	. ${library_path}/file_parsing/array_check.sh
	. ${library_path}/file_parsing/array_extract.sh
else
	echo "Can't load library files, abort"
	exit 1
fi

# Trap CTRL C and execute gracefulExit when catch it
trap gracefulExit INT

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

# Check if one argument have been provide
if [ ${#1} -eq 0 ]
then
	echo "No argument was provided, abort"
	exit 1
fi

# Check if argument given if a file
if [ ! -f "$1" ]
then
	echo "No valid filename given."
	exit 1
fi

DEBUG=2
new_file="worker_conf.clean"
log_file="dev_to_prod.log"

# Declare all valid keywork
declare -a keywords
keywords=("preprod-worker" "preprod-user" "preprod-path" "prod-user" "prod-path")
keywords_array=("branch" "merge-options" "prod-workers")
keywords_mand=("branch" "preprod-user" "preprod-worker" "preprod-path" "prod-user" "prod-path" "prod-workers")

# Check if argument
if [ ${#1} -eq 0 ]
then
	echo "No argument was provided, abort"
	exit 1
fi

# Check if file given exist
if [ -d "$1" ]
then
	echo "No valid filename given."
	exit 1
fi

# Remove duplicate and create array to compare the 2 data contents
no_duplicate=$(cat "$1" | sort -u | sed '/^\s*$/d')
readarray -t no_dupli_arr <<<"$no_duplicate"
file_content=$(cat "$1" | sort | sed '/^\s*$/d')
readarray -t file_arr <<<"$file_content"

# Return diff between 2 arrays
array_diff=$(printf '%s\n' "${no_dupli_arr[@]}" "${file_arr[@]}" | sort | uniq -u)

# If duplicate, create new file with clean lines
if [ ${#array_diff} -eq 0 ]
then
	echo -e ${RED}"Duplicate find in file, create fresh new file"${NC}
	echo "$no_duplicate" > "$1.no"
	# Put all line in array, and trim
	readarray file_content_arr < "$1".no
else
	readarray file_content_arr < "$1"
fi

# Unset unused variables
unset no_duplicate file_content

declare -a file_content

# Delele and recreate new file for push clean line into
if [ -f ${new_file} ]
then
	rm ${new_file}
fi
touch ${new_file}

# Parse file given in parameter, line by line
for i in "${!file_content_arr[@]}"
do
	check_next_line "${file_content_arr[$i]}"
done

# If $keywords_mand array contain at least one value
# Not all mandatory keywords have been provide so abort
if [ "${#keywords_mand[@]}" != 0 ]
then
	echo -e ${RED}"Not all mandatory keywords have been provided, missing:"
	echo -e ${keywords_mand[@]}${NC}
	exit 1
fi

# if check_next_line has never exit script due to bad line format in argv file,
# We get clean array from new file created
readarray clean_array < ${new_file}

# Now we get back variable from argument file
keyword='branch'
if ft_valid_keyword "${keyword}"
then
	branch_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

keyword='merge-options'
if ft_valid_keyword "${keyword}"
then
	merge_options_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

keyword='preprod-user'
if ft_valid_keyword "${keyword}"
then
	preprod_user_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='preprod-worker'
if ft_valid_keyword "${keyword}"
then
	preprod_worker_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='preprod-path'
if ft_valid_keyword "${keyword}"
then
	preprod_path_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='prod-user'
if ft_valid_keyword "${keyword}"
then
	prod_user_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

keyword='prod-workers'
if ft_valid_keyword "${keyword}"
then
	prod_workers_val=($(ft_get_value "$keyword" "${clean_array[@]}"))
fi

keyword='prod-path'
if ft_valid_keyword "${keyword}"
then
	prod_path_val=$(ft_get_value "$keyword" "${clean_array[@]}")
fi

# if DEBUG>0 show all key and values getting form file
if [ $DEBUG -gt 0 ]
then
	echo -e ${YEL}"Show all val from argument file"
	for i in "${!keywords[@]}"
	do
		value=$(ft_get_value "${keywords[$i]}" "${clean_array[@]}")
		echo ${keywords[$i]}": $value"
	done
	echo -e "${NC}"
fi

# Get all value from array
merge_options_arr=($(ft_get_value_arr "${merge_options_val}"))

# Check ipv4 workers
if ! ft_is_ipv4 "${preprod_worker_val}"
then
	echo "Bad IPv4 for 'preprod_worker', abort"
	exit 1
fi
echo "IPv4 preprod-worker: OK"

# Get all value from array
prod_workers_arr=($(ft_get_value_arr "${prod_workers_val}"))

# Check ipv4 from prod_worker_arr
for i in "${!prod_workers_arr[@]}"
do
	if ! ft_is_ipv4 "${prod_workers_arr[$i]}"
	then
		echo -e ${RED}"Bad IPv4 "${prod_workers_val[$i]}" for 'prod_workers_val', abort"${NC}
		exit 1
	fi
done
echo "IPv4 prod-worker: OK"

# test ssh connection to preprod-worker 
ssh_return=$(ssh "${preprod_user_val}"@"${preprod_worker_val}" 'exit 2>&1')

# If ssh_return != "" abort
if [ ${#ssh_return} -ne 0 ]
then
	# If DEBUG>0 print info
	if [ $DEBUG -gt 0 ]
	then
		echo "ssh return:"
		echo "$ssh_return"
	fi
	echo -e ${RED}"Can't connect to preprod worker via '${preprod_worker_val}' IPv4, abort"${NC}
	exit 1
fi
echo "ssh preprod-worker: OK"

# If function return nothing abort
if [ ${#prod_workers_val} -eq 0 ]
then
	echo -e ${RED}"Function ft_get_value_arr() exit with code 1, check 'prod_workers_val' value"${NC}
	exit 1
fi

# Test ssh connection for every prod_workers
for i in "${!prod_workers_arr[@]}"
do
	# test ssh connection to preprod-worker 
	echo -e ${YEL}ssh "${prod_user_val}"@"${prod_workers_arr[$i]}" 'exit 2>&1'${NC}
	ssh_return=$(ssh "${prod_user_val}"@"${prod_workers_arr[$i]}" 'exit 2>&1')

	# If ssh_return != "" abort
	if [ ${#ssh_return} -ne 0 ]
	then
		# If DEBUG>0 print info
		if [ $DEBUG -gt 0 ]
		then
			echo "ssh return:"
			echo "$ssh_return"
		fi
		echo -e ${RED}"Can't connect to prod worker via '${prod_workers_arr[$1]}' IPv4, abort"${NC}
		exit 1
	fi
done
echo "ssh prod-workers: OK"

# Check that preprod_path exist
echo -e ${YEL}ssh "${preprod_user_val}"@"${preprod_worker_val}" stat $preprod_path_val \> /dev/null 2\>\&1${NC}
path_exist=$(ssh "${preprod_user_val}"@"${preprod_worker_val}" stat $preprod_path_val \> /dev/null 2\>\&1)

if [ "$?" != 0 ]
then
	echo -e ${RED}"Path '$preprod_path_val' doesn't exist on remote preprod-worker '$preprod_worker_val', abort"${NC}
	exit 1
fi
echo "worker path on preprod: OK"

# Get all value from array
branch_arr=($(ft_get_value_arr "${branch_val}"))

# If function return nothing abort
if [ ${#prod_workers_val} -eq 0 ]
then
	echo -e ${RED}"Function ft_get_value_arr() exit with code 1, check 'branch_val' value"${NC}
	exit 1
fi

# Connect to preprod worker and check if the 2 branch exist
for i in ${!branch_arr[@]}
do
	echo -e ${YEL}ssh "${preprod_user_val}"@"${preprod_worker_val}" "cd $preprod_path_val && git branch | grep -oP '${branch_arr[$i]}'"${NC}
	branch_exit=$(ssh "${preprod_user_val}"@"${preprod_worker_val}" "cd $preprod_path_val && git branch | grep -oP '${branch_arr[$i]}'")
	# If DEBUG>0 print info
	if [ $DEBUG -gt 0 ]
	then
		echo -e ${YEL}"branch exit return: ${branch_exit}"${NC}
	fi

	# If ssh return nothing exit
	if [ "${#branch_exit}" -eq 0 ]
	then
		echo -e ${RED}"Git branch '$branch_val[$i]' isn't on remote preprod-worker ${preprod_worker}"${NC}
		exit 1
	fi
done
echo "branch: OK"

# Merge 2nd branch with first
echo -e ${YEL}ssh "${preprod_user_val}"@"${preprod_worker_val}" "cd $preprod_path_val && git checkout ${branch_arr[0]} && git merge ${merge_options_arr[0]} ${branch_arr[1]}"${NC}
ssh_merge_return=$(ssh "${preprod_user_val}"@"${preprod_worker_val}" "cd $preprod_path_val && git checkout ${branch_arr[0]} && git merge ${merge_options_arr[0]} ${branch_arr[1]}")
echo "$ssh_merge_return"

# Check if merge return 'Fast-forward' or 'Already up-to-date'
merge_status=$(echo "$ssh_merge_return" | grep -Po 'file changed|Already up-to-date')
if [ "$merge_status" == 'file changed' ]
then
	echo "Merge: OK"
	# Push previous merge
	echo -e ${YEL}ssh "${preprod_user_val}"@"${preprod_worker_val}" 'cd "'$preprod_path_val'" && git push origin master; echo $?'${NC}
	git_push_return=$(ssh "${preprod_user_val}"@"${preprod_worker_val}" 'cd "'$preprod_path_val'" && git push origin master; echo $?')

	# Check if push was execute succefully
	if [ "$?" != 0 ]
	then
		echo -e ${RED}"Error when pushing merge, abort"${NC}
		exit 1
	fi
elif [ "$merge_status" == 'Already up-to-date' ]
then
	# Nothing to do exit 0
	echo -e ${GRE}"Everythings is update, nothing to push"${NC}
	exit 0
else
	# Error in merge exit 1
	echo -e ${RED}"Merge return unknow value"${NC}
	echo "$merge_status"
	exit 1	
fi
echo "Push: OK"

# Loop on every worker for pulling new data from gogs
for i in ${!prod_workers_arr[@]}
do
	echo -e ${YEL}ssh "${prod_user_val}"@"${prod_workers_arr[$i]}" 'cd "'$prod_path_val'" && git pull; echo $?'${NC}
	git_pull_return=$(ssh "${prod_user_val}"@"${prod_workers_arr[$i]}" 'cd "'$prod_path_val'" && git pull; echo $?')

	# If check pull return status
	if [ "$?" == 0 ]
	then
		echo "Repository of worker: '${prod_workers_arr[$i]}' was succefully update"
	# If error unknow, abort
	else
		echo -e ${RED}"Unkown error return by git pull of worker: '${prod_workers_arr[$i]}', abort"${NC}
		exit 1
	fi
done
echo "Update of all prod worker: OK"

# test ssh connection from worker1 to worker2 
ssh_worker1_return=$(ssh "${prod_user_val}"@"${prod_workers_arr[0]}" ssh "${prod_user_val}"@"${prod_workers_arr[1]}" 'exit')

# If ssh_return != "" abort
if [ $? != 0 ]
then
	# If DEBUG>0 print info
	if [ $DEBUG -gt 0 ]
	then
		echo "ssh worker1 return:"
		echo "$ssh_worker1_return"
	fi
	echo -e ${RED}"Can't connect from prod '${prod_workers_arr[1]}' via '${prod_workers_arr[0]}' IPv4, abort"${NC}
	exit 1
fi

# test ssh connection from worker2 to worker1
ssh_worker2_return=$(ssh "${prod_user_val}"@"${prod_workers_arr[1]}" ssh "${prod_user_val}"@"${prod_workers_arr[0]}" 'exit')

# If ssh_return != "" abort
if [ $? != 0 ]
then
	# If DEBUG>0 print info
	if [ $DEBUG -gt 0 ]
	then
		echo "ssh worker2 return:"
		echo "$ssh_worker2_return"
	fi
	echo -e ${RED}"Can't connect from prod '${prod_workers_arr[0]}' via '${prod_workers_arr[1]}' IPv4, abort"${NC}
	exit 1
fi

#for i in ${!prod_workers_arr[@]}
#do
	# Check entire content of all prod worker
	echo -e ${YEL}ssh "${prod_user_val}"@"${prod_workers_arr[0]}" "rsync -ani --size-only --delete --exclude=.git ${prod_path_val}/ ${prod_user_val}@${prod_workers_arr[1]}:${prod_path_val} | grep '^<' | awk "'{ print $2 }'${NC}
	rsync_diff_return1=$(ssh "${prod_user_val}"@"${prod_workers_arr[0]}" rsync -ani --size-only --delete --exclude=.git ${prod_path_val}/ ${prod_user_val}@${prod_workers_arr[1]}:${prod_path_val} | grep '^<' | awk '{ print $2 }')
	rsync_diff_return2=$(ssh "${prod_user_val}"@"${prod_workers_arr[1]}" rsync -ani --size-only --delete --exclude=.git ${prod_path_val}/ ${prod_user_val}@${prod_workers_arr[0]}:${prod_path_val} | grep '^<' | awk '{ print $2 }')
#done

# If DEBUG>0 print info
if [ $DEBUG -gt 0 ]
then
	echo "rsync worker1 return:"
	echo "$rsync_diff_return1"
fi

# If diff return contain more than one line, problem between the worker occur, abort
if [ ${#rsync_diff_return1} -ne 0 ] || [ ${#rsync_diff_return2} -ne 0 ]
then
	echo -e ${RED}"Tree file miss match between '${prod_workers_arr[0]}' and '${prod_workers_arr[1]}', abort"${NC}
	exit 1
fi
echo "Workers tree directory: OK"
